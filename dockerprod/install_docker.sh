#!/bin/bash

## install docker

IP=$(hostname -I | awk '{print $2}')

echo "START - install docker -"$IP

echo "[1]: install utils & docker"

    sudo apt -y update && sudo apt -y upgrade
    sudo apt -y install apt-transport-https ca-certificates curl gnupg2 software-properties-common
    sudo curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add - 
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
    sudo apt-get update
    sudo apt-get install docker-ce docker-ce-cli containerd.io
    sudo echo 'DOCKER_OPTS="-H tcp://127.0.0.1:4243 -H unix:///var/run/docker.sock"' >> /etc/default/docker
    sudo service docker restart
    sudo usermod -aG docker vagrant
    echo 'export DOCKER_HOST=tcp://localhost:4243' >> /home/vagrant/.bashrc
    curl -L https://github.com/docker/compose/releases/download/1.6.2/docker-compose-`uname -s`-`uname -m` -o docker-compose
    sudo chmod +x docker-compose
    sudo mv docker-compose /usr/local/bin

echo "END - install docker"

